﻿namespace APICuet.Models
{
    public class CUT_UniversityPostModel
    {
        public string UniversityFullName { get; set; } = null;
        public string? UniversityShortName { get; set; } = null;
        public string? Website { get; set; } = null;

        public int UniversityTypeID { get; set; }

        public string? Remark { get; set; } = null;     
    }
}
