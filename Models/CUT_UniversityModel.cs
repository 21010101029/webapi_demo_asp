﻿namespace APICuet.Models
{
    public class CUT_UniversityModel
    {

        public int UniversityID { get; set; }   
        public string? UniversityFullName { get; set; }
        public string? UniversityShortName { get; set; }
        public string? Website { get; set; }

        public int UniversityTypeID { get; set; }

        public string? Remark { get; set; }

    }
}
