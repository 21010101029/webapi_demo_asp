﻿using APICuet.Models;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Newtonsoft.Json;
using System.Data;
using System.Data.Common;
using System.Reflection;
using System.Text.Json;

namespace APICuet.DAL.CUT_UniversityDAL
{
    public class CUT_UniversityDALBase : DALHelper
    {
        #region METHOD: PR_CUT_University_SelectALL
        public List<CUT_UniversityModel> PR_CUT_University_SelectALL()
        {
            try
            {
                SqlDatabase sqlDatabase = new SqlDatabase(MyConnection);
                DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_CUT_University_SelectALL");
                DataTable dt = new DataTable();
                List<CUT_UniversityModel> list = new List<CUT_UniversityModel>();
                using (IDataReader reader = sqlDatabase.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        CUT_UniversityModel model = new CUT_UniversityModel();
                        model.UniversityID = Convert.ToInt32(reader["UniversityID"]);
                        model.UniversityFullName = reader["UniversityFullName"].ToString();
                        model.UniversityShortName = reader["UniversityShortName"].ToString();
                        model.Remark = reader["Remark"].ToString();
                        model.UniversityTypeID = Convert.ToInt32(reader["UniversityTypeID"]);
                        model.Website = reader["Website"].ToString();
                        list.Add(model);
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region METHOD: PR_CUT_University_SelectByID
        public CUT_UniversityModel PR_CUT_University_SelectByID(int UniversityID)
        {
            try
            {
                SqlDatabase sqlDatabase = new SqlDatabase(MyConnection);
                DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_CUT_University_SelectByID");
                sqlDatabase.AddInParameter(dbCommand, "@UniversityID", DbType.Int32, UniversityID);
                CUT_UniversityModel model = new CUT_UniversityModel();
                using (IDataReader reader = sqlDatabase.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        model.UniversityID = Convert.ToInt32(reader["UniversityID"]);
                        model.UniversityFullName = reader["UniversityFullName"].ToString();
                        model.Remark = reader["Remark"].ToString();
                        model.UniversityShortName = reader["UniversityShortName"].ToString();
                        model.UniversityTypeID = Convert.ToInt32(reader["UniversityTypeID"]);
                        model.Website = reader["Website"].ToString();

                    }
                }
                return model;
            }
            catch (Exception ex)
            {
                return null;
            }

        }
        #endregion

        #region METHOD: PR_CUT_University_Insert
        public void PR_CUT_University_Insert(CUT_UniversityPostModel postmodel)
        {
            try
            {
                SqlDatabase sqlDatabase = new SqlDatabase(MyConnection);
                DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_CUT_University_Insert");
                sqlDatabase.AddInParameter(dbCommand, "@UniversityFullName", DbType.String, postmodel.UniversityFullName);
                sqlDatabase.AddInParameter(dbCommand, "@UniversityShortName", DbType.String, postmodel.UniversityShortName);
                sqlDatabase.AddInParameter(dbCommand, "@Website", DbType.String, postmodel.Website);
                sqlDatabase.AddInParameter(dbCommand, "@UniversityTypeID", DbType.Int32, postmodel.UniversityTypeID);
                sqlDatabase.AddInParameter(dbCommand, "@Remark", DbType.String, postmodel.Remark);
                sqlDatabase.ExecuteNonQuery(dbCommand);
                return;
            }
            catch (Exception ex)
            {
                return ;
            }

        }
        #endregion

        #region MEHOD: PR_CUT_University_DeleteByID
        public bool PR_CUT_University_DeleteByID(int UniversityID)
        {
            try
            {
                SqlDatabase sqlDatabase= new SqlDatabase(MyConnection);
                DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_CUT_University_DeleteByID");
                sqlDatabase.AddInParameter(dbCommand, "@UniversiyID", DbType.String, UniversityID);
                sqlDatabase.ExecuteNonQuery(dbCommand);
                return true ;

            }catch (Exception ex) {
                return false ;
            }
        }
        #endregion

        #region MEHOD: PR_CUT_University_UpdateByID
        public bool PR_CUT_University_UpdateByID(int ID,CUT_UniversityPostModel postmodel)
        {
            try
            {
                SqlDatabase sqlDatabase=new SqlDatabase(MyConnection);
                DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_CUT_University_UpdateByID");
                sqlDatabase.AddInParameter(dbCommand, "@UniversityID", DbType.Int32, ID);
                sqlDatabase.AddInParameter(dbCommand, "@UniversityFullName", DbType.String, postmodel.UniversityFullName);
                sqlDatabase.AddInParameter(dbCommand, "@UniversityShortName", DbType.String, postmodel.UniversityShortName);
                sqlDatabase.AddInParameter(dbCommand, "@Website", DbType.String, postmodel.Website);
                sqlDatabase.AddInParameter(dbCommand, "@UniversityTypeID", DbType.Int32, postmodel.UniversityTypeID);
                sqlDatabase.AddInParameter(dbCommand, "@Remark", DbType.String, postmodel.Remark);
                sqlDatabase.ExecuteNonQuery(dbCommand);
                return true;

            }
            catch (Exception ex) {
                return false;
            }
        }
        #endregion
    }
}
