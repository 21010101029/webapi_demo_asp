﻿using APICuet.DAL.CUT_UniversityDAL;
using APICuet.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace APICuet.Controllers
{

    [Route("api/[controller]")] 
    [ApiController]
    public class CUT_UniversityController : Controller
    {
        CUT_UniversityDAL dal = new CUT_UniversityDAL();

        #region PR_CUT_University_SelectALL
        [HttpGet]
        public IActionResult Get()
        {
            List<CUT_UniversityModel> model = dal.PR_CUT_University_SelectALL();
            Dictionary<string, dynamic> response = new Dictionary<string, dynamic>();
            if (model.Count > 0 && model!= null)
            {
                response.Add("status", true);
                response.Add("message", "Data Found");
                response.Add("data", model);
                return Ok(response);
            }
            else
            {
                response.Add("status", false);
                response.Add("message", "Data Not Found");
                response.Add("data", null);
                return NotFound(response);
            }
        }
        #endregion

        #region PR_CUT_University_SelectByID
        [HttpGet("{UniversityID}")]
        public IActionResult Index(int UniversityID)
        {

            CUT_UniversityModel model = dal.PR_CUT_University_SelectByID(UniversityID);
            Dictionary<string, dynamic> response = new Dictionary<string, dynamic>();
            if (model != null)
            {
                response.Add("status", true);
                response.Add("message", "Data Found");
                response.Add("data", model);
                return Ok(response);
            }
            else
            {
                response.Add("status", false);
                response.Add("message", "Data Not Found");
                response.Add("data", null);
                return NotFound(response);
            }

        }

        #endregion

        #region PR_CUT_University_Insert
        [HttpPost()]
        public IActionResult Index([FromBody] CUT_UniversityPostModel model)
        {
            if (model == null)
            {
                return BadRequest("Invalid data");
            }

            CUT_UniversityPostModel postmodel = new CUT_UniversityPostModel();
            postmodel.UniversityFullName=model.UniversityFullName;
            postmodel.UniversityShortName=model.UniversityShortName;
            postmodel.UniversityTypeID=model.UniversityTypeID;
            postmodel.Website=model.Website;
            postmodel.Remark=model.Remark;
            dal.PR_CUT_University_Insert(postmodel);

            return CreatedAtAction(nameof(Get), new { id = "" }, model);


        }

        #endregion

        [HttpPut("{UniversityID}")]
        public IActionResult PR_CUT_University_UpdateByID(int UniversityID, [FromBody] CUT_UniversityPostModel model)
        {
            if (model == null)
            {
                return BadRequest("Invalid data");
            }
            Dictionary<string, dynamic> response = new Dictionary<string, dynamic>();
            if (dal.PR_CUT_University_UpdateByID(UniversityID, model))
            {
                response.Add("status", true);
                response.Add("message", "Data Update SuccessFully");
                return Ok(response);
            }
            else
            {
                response.Add("status", false);
                response.Add("message", "Data Not Found ");
                return NotFound(response);
            }

        }

        #region PR_CUT_University_DeleteByID
        [HttpDelete("{UniversityID}")]
        public IActionResult PR_CUT_University_DeleteByID(int UniversityID)
        {
            
            Dictionary<string, dynamic> response = new Dictionary<string, dynamic>();
            if (dal.PR_CUT_University_DeleteByID(UniversityID))
            {
                response.Add("status", true);
                response.Add("message", "Data Found");
                response.Add("data", "SuccessFully Deleted");
                return Ok(response);
            }
            else
            {
                response.Add("status", false);
                response.Add("message", "Data Not Found");
                response.Add("data", null);
                return NotFound(response);
            }

        }
        #endregion





    }
}
