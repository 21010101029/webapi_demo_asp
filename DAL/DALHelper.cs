﻿namespace APICuet.DAL
{
    public class DALHelper
    {
        #region ConnectionString 
        public static string MyConnection = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetConnectionString("myConnectionString");
        #endregion
    }
}
